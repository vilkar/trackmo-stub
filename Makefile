export VBCC=tools/vbcc
export PATH := tools/vbcc/bin:$(PATH)
VASM=tools/vasm/vasmm68k_mot -Ivendor/NDK_3.9/Include/include_i -quiet
VC=$(VBCC)/bin/vc -c99 -Itools/vbcc/targets/m68k-kick13/include -Ivendor/NDK_3.9/Include/include_h
VLINK=tools/vlink/vlink -M -k -s -Z
HXCFE=tools/hxc/HxCFloppyEmulator_cmdline/build/hxcfe
XDFTOOL=tools/amitools/bin/xdftool

ASMOBJS=hwstart.o lz4_smallest.o p61.o data.o
NAME=trackmo
DISK=$(NAME).adf
TARGET=$(NAME).exe

.PHONY: help

help:
	@echo "Available make targets:"
	@echo
	@echo "all               Build tools, vendor, bootblock and executable"
	@echo
	@echo "$(NAME)            Build and run $(NAME)"
	@echo "clean             Clean build"
	@echo
	@echo "tools             Build all tools"
	@echo "tools-clean       Clean tool builds"
	@echo
	@echo "vendor-all        Build vendor includes"
	@echo "vendor-clean      Clean vendor include build"
	@echo

all: tools vendor $(TARGET) $(DISK)

%.o: %.asm
	$(VASM) -o $@ -Fhunk -kick1hunks $^

%.o: %.c
	$(VC) +kick13m -c -O3 -nostdlib -o $@ $^

$(TARGET): $(NAME).o datafiles $(ASMOBJS)
	$(VLINK) -o $@ $(NAME).o -set-adduscore $(ASMOBJS)
	ls -l $(TARGET)

$(DISK): $(TARGET)
	$(XDFTOOL) $(DISK) format $(NAME)
	$(XDFTOOL) $(DISK) boot install boot1x
	echo $(TARGET) > startup-sequence
	$(XDFTOOL) $(DISK) makedir S
	$(XDFTOOL) $(DISK) write startup-sequence S/Startup-Sequence
	rm startup-sequence
	$(XDFTOOL) $(DISK) write $(NAME).exe

datafiles:
	make -C data

$(NAME): $(DISK)
	fs-uae $(NAME).fs-uae

hfe: $(DISK)
	$(HXCFE) -finput:$(DISK) -foutput:bootblock.hfe -conv:HXC_HFE $(NAME).hfe

sd: hfe
	mcopy $(NAME).hfe -o z:autoboot.hfe
	sync

clean:
	rm -f $(TARGET) $(DISK) *.o *~ *.hfe

toolcheck:
	stat tools/vasm tools/vbcc tools/vbcc_target_m68k-kick13 tools/vlink tools/dmlib tools/amitools/bin tools/hxc/libhxcadaptor/build tools/hxc/libusbhxcfe/build tools/hxc/libhxcfe/build tools/hxc/HxCFloppyEmulator_cmdline/build >/dev/null || \
	(echo Please uncompress tools and/or initialize submodules! && false)

tools: toolcheck
	make -C tools/bootchecksum
	touch tools/dmlib/config.mak
	make -C tools/dmlib tools/gfxconv DM_USE_STDIO=yes DM_USE_LIBPNG=yes
	make -C tools/vasm CPU=m68k SYNTAX=mot
	make -C tools/vlink
	mkdir -p tools/vbcc/bin tools/vbcc/targets
	cd tools/vbcc/targets; ln -sf ../../vbcc_target_m68k-kick13/targets/m68k-kick13 .
	cd tools/vbcc/bin; ln -sf ../../vasm/vasmm68k_mot ../../vlink/vlink .
	tar -C tools/vbcc -xvf tools/vbcc_unix_config.tar.gz
	yes '' | make -C tools/vbcc TARGET=m68k
	make -C tools/hxc/libhxcadaptor/build
	make -C tools/hxc/libusbhxcfe/build
	make -C tools/hxc/libhxcfe/build
	make -C tools/hxc/HxCFloppyEmulator_cmdline/build
	make -C tools/amitools build

tools-clean:
	make -C tools/bootchecksum clean
	make -C tools/dmlib clean
	make -C tools/vasm clean
	rm -f tools/vlink/objects/* tools/vlink/vlink
	make -C tools/hxc/libhxcadaptor/build mrproper
	make -C tools/hxc/libusbhxcfe/build mrproper
	make -C tools/hxc/libhxcfe/build mrproper
	make -C tools/hxc/HxCFloppyEmulator_cmdline/build mrproper
	make -C tools/amitools clean

vendorcheck:
	stat vendor/NDK_3.9 vendor/fd2pragma >/dev/null || \
	(echo Please uncompress vendor archives! && false)

vendor: vendorcheck
	-make -C vendor/fd2pragma clean
	make -C vendor/fd2pragma
	cd vendor/fd2pragma && mkdir -p ../NDK_3.9/Include/include_i/lvo/ ../NDK_3.9/Include/include_h/proto/ ../NDK_3.9/Include/include_h/inline/ && \
	for fdfile in ../NDK_3.9/Include/fd/*.fd; do \
		./fd2pragma -i $$fdfile --special 23 --to ../NDK_3.9/Include/include_i/lvo/; \
		./fd2pragma -i $$fdfile --special 38 --to ../NDK_3.9/Include/include_h/proto/; \
		./fd2pragma -i $$fdfile --special 70 --voidbase --to ../NDK_3.9/Include/include_h/inline/ --clib ../NDK_3.9/Include/include_h/clib/`basename $$fdfile _lib.fd`_protos.h; \
	done;

vendor-clean:
	-make -C vendor/fd2pragma clean
	-rm -rv vendor/NDK_3.9/Include/include_i/lvo
