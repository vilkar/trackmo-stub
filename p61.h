extern void P61_Init(__reg("a0") uint8_t *module,__reg("a1") uint8_t *samples,__reg("a4") uint8_t *dmapoke);
extern void P61_Music();
extern void P61_End();
extern volatile uint16_t P61_Pos;
extern volatile uint16_t P61_CRow;
