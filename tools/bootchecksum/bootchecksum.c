#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

void croak(char *str)

{
	int error=errno;
	fprintf(stderr,"ERROR: %s\n",str);
	if (error) {
		fprintf(stderr,"DEBUG: Last error (errno) %i: %s\n",error,strerror(error));
	}
	exit(EXIT_FAILURE);
}

int main(int argc,char *argv[])

{
	unsigned char bootblock[880*1024];
	if (argc!=2) {
		croak("SYNTAX: bootchecksum <file>");
	}
	int fd=open(argv[1],O_RDWR);
	memset(bootblock,0,sizeof(bootblock));
	read(fd,bootblock,880*1024);

	bootblock[4]=0;
	bootblock[5]=0;
	bootblock[6]=0;
	bootblock[7]=0;
	uint32_t o;
	uint32_t c=0;
	int i;
	for (i=0; i<1024; i+=4) {
		o=c;
		c+=(bootblock[i]<<24)+(bootblock[i+1]<<16)+(bootblock[i+2]<<8)+bootblock[i+3];
		if (c<o) {
			++c; // carry
		}
	}
	c=~c;
	bootblock[4]=(c>>24)&0xff;
	bootblock[5]=(c>>16)&0xff;
	bootblock[6]=(c>>8)&0xff;
	bootblock[7]=(c)&0xff;
	lseek(fd,0,SEEK_SET);
	write(fd,bootblock,880*1024);
	close(fd);
}
