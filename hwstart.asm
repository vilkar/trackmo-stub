	ifnd custom
custom	equ	$dff000
	endc

	include	graphics/gfxbase.i
	include hardware/custom.i
	include lvo/exec_lib.i
	include lvo/graphics_lib.i

	xref	hwstart
	xref	hwend

hwstart:
	movea.l	4.w,a6
	lea	hwstart_gfxlibstr,a1
	moveq	#33,d0
	jsr	_LVOOpenLibrary(a6)
	move.l	d0,hwstart_gfxbase
	beq	exit
	move.l	d0,a6
	move.l	gb_ActiView(a6),hwstart_oldview
	sub.l	a1,a1
	jsr	_LVOLoadView(a6)
	jsr	_LVOWaitTOF(a6)
	jsr	_LVOWaitTOF(a6)
	rts

hwend:
	move.l	hwstart_gfxbase,a6
	sub.l	a1,a1
	jsr	_LVOLoadView(a6)
	move.l	hwstart_oldview,a1
	jsr	_LVOLoadView(a6)
	move.l	gb_copinit(a6),(custom+cop1lc)
	jsr	_LVOWaitTOF(a6)
	jsr	_LVOWaitTOF(a6)	
	move.l	a6,a1
	move.l	4.w,a6
	jsr	_LVOCloseLibrary(a6)
exit:
	moveq.l	#0,d0
	rts

hwstart_gfxlibstr:
	dc.b	'graphics.library',0,0
hwstart_oldview	
	dc.l	0
hwstart_gfxbase	
	dc.l	0
