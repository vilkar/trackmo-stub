#include <exec/interrupts.h>
#include <hardware/cia.h>
#include <hardware/custom.h>
#include <hardware/dmabits.h>
#include <hardware/intbits.h>
#include <proto/exec.h>
#include <stddef.h>
#include <stdint.h>
#include "data.h"
#include "hwstart.h"
#include "lz4_smallest.h"
#include "p61.h"

volatile struct CIA *ciaa=(struct CIA *)0xbfe001;
volatile struct Custom *custom=(struct Custom *)0xdff000;
struct ExecBase *SysBase;
volatile uint32_t framecount;

int main();

void sysintretval_asm()=
"	moveq.l	#0,d0 			\n";

void copint_asm()=
"	move.l	a1,a6			\n"
"	movem.l	d2-d7/a2-a4,-(sp) 	\n"
"	jsr	_P61_Music		\n"
"	movem.l	(sp)+,d2-d7/a2-a4	\n";

__amigainterrupt void copint()
{
	custom->color[0]=0xfff;
	copint_asm();
	custom->color[0]=0x666;
	sysintretval_asm();
}

struct Interrupt copintserver={
	{NULL,NULL,NT_INTERRUPT,127,"p61"},
	(struct Custom *)0xdff000,
	copint
};

__amigainterrupt void vrtint()
{
	custom->dmacon=DMAF_SPRITE;
	framecount++;
	custom->color[0]=framecount;
	sysintretval_asm();
}

struct Interrupt vrtintserver={
	{NULL,NULL,NT_INTERRUPT,127,"vrt"},
	(struct Custom *)0xdff000,
	vrtint
};

#define creg(x)	offsetof(struct Custom,x)
__chip uint16_t copperlist[]={
	creg(bplcon0),	0x1200,
	creg(bplcon1),	0,
	creg(bplcon2),	0,
	creg(bplcon3),	0x0c00,
	creg(bplcon4),	0x0011,
	creg(bpl1mod),	0,
	creg(bpl2mod),	0,
	creg(ddfstrt),	0x0038,
	creg(ddfstop),	0x00d0,
	creg(diwstrt),	0x2c81,
	creg(diwstop),	0x2cc1,
	creg(fmode),	0,
	creg(bplpt),	0,
	creg(bplpt)+2,	0,
	creg(color),	0,
	creg(color)+2,	0x0fff,
	0x8007,		0xfffe,
	creg(color), 	0x0666,
	creg(intreq),	0x8010,
	0x8b07,		0xfffe,
	creg(dmacon),	0x8000,
	creg(color),	0,
	0xffff,		0xfffe,
};
uint16_t *bplptr=&copperlist[24];

__chip uint8_t mod[1310];
__chip uint8_t screen[10240];

int main()

{
	SysBase=*(struct ExecBase **)4;
	for (uint16_t i=0; i<10240/4; i++) {
		((uint32_t *)screen)[i]=0;
	}
	bplptr[1]=(intptr_t)screen>>16;
	bplptr[3]=(intptr_t)screen&0xffff;
	lz4_depack(modz,modz_end,mod);
	hwstart();
	AddIntServer(INTB_VERTB,&vrtintserver);
	P61_Init(mod,NULL,(uint8_t *)copperlist+4+79);
	AddIntServer(INTB_COPER,&copintserver);
	custom->cop1lc=(intptr_t)copperlist;
	lz4_depack(afwdlogoz,afwdlogoz_end,screen);
	do {
		custom->color[0]=(P61_Pos<<8)+(P61_CRow>>2);
	} while (ciaa->ciapra & (1<<CIAB_GAMEPORT0));

	RemIntServer(INTB_COPER,&copintserver);
	P61_End();
	RemIntServer(INTB_VERTB,&vrtintserver);
	hwend();
	return 0;
}
